include ../VERSION
 
BUILDDIR=build
DEFAULT_KEY=$(shell awk '/^default-key/ { print $$2 }' $(HOME)/.gnupg/options)
DISTTAG ?= $(shell lsb_release -r  | sed -nr 's/^[^[:space:]]+[[:space:]]+([0-9]+)\.([0-9]+)$$/.slc\1/p' )
RPMOPTS=
#OSMAJRELEASE=$(shell sed -e 's/.*release \([0-9]\+\).*/\1/g' /etc/redhat-release )
#CENTOS=$(shell test $(OSMAJRELEASE) -gt 6 && echo true)

# temp. comment out (old build way reqs disttag predefined)
#ifeq ($(CENTOS),true)
#	DISTTAG=.el$(OSMAJRELEASE).cern
#else
#	DISTTAG=.slc$(OSMAJRELEASE)
#endif

ifneq ($(DISTTAG), .el7.cern)
ifneq ($(DISTTAG), .slc6)
ifneq ($(DISTTAG), .slc5)
$(error Only SLC5/SLC6/CC7 builds are supported.)
endif
endif
endif

ifeq ($(DISTTAG), .slc5)
RPMOPTS=--define '_source_filedigest_algorithm 1' --define '_binary_filedigest_algorithm 1' --define '_binary_payload w9.gzdio'
endif

TARBALL=../$(PACKAGE)-$(VERSION).tar.gz
 
default: rpm
 
rpmprep:
	mkdir -p $(BUILDDIR)
	mkdir -p $(BUILDDIR)/BUILD
	mkdir -p $(BUILDDIR)/RPMS
	mkdir -p $(BUILDDIR)/SRPMS
	mkdir -p $(BUILDDIR)/SOURCES
	mkdir -p $(BUILDDIR)/SPECS
	cp $(PACKAGE).spec $(BUILDDIR)/SPECS/
	sed -i 's/@PACKAGE@/$(PACKAGE)/g' $(BUILDDIR)/SPECS/$(PACKAGE).spec
	sed -i 's/@VERSION@/$(VERSION)/g' $(BUILDDIR)/SPECS/$(PACKAGE).spec
	sed -i 's/@RELEASE@/$(RELEASE)/g' $(BUILDDIR)/SPECS/$(PACKAGE).spec
	sed -i 's/@EMAIL@/$(EMAIL)/g' $(BUILDDIR)/SPECS/$(PACKAGE).spec

$(TARBALL):
	make -C .. tar
 
tarprep: $(TARBALL) rpmprep
	cp $(TARBALL) $(BUILDDIR)/SOURCES

stamp-srpm: tarprep $(TARBALL)
	cd $(BUILDDIR); rpmbuild -bs $(RPMOPTS) --define "_topdir $(CURDIR)/$(BUILDDIR)" --define "dist $(DISTTAG)" SPECS/$(PACKAGE).spec
	cp $(BUILDDIR)/SRPMS/*.rpm .
	touch stamp-srpm

srpm: stamp-srpm

stamp-brpm: tarprep $(TARBALL)
	cd $(BUILDDIR); rpmbuild -bb $(RPMOPTS) --define "_topdir $(CURDIR)/$(BUILDDIR)" --define "dist $(DISTTAG)" SPECS/$(PACKAGE).spec
	cp $(BUILDDIR)/RPMS/*/*.rpm .
	touch stamp-brpm

brpm: stamp-brpm

stamp-rpm: srpm brpm
	touch stamp-rpm

rpm: stamp-rpm

stamp-rpmsign: rpm
	if [ -n "$(DEFAULT_KEY)" ]; then rpm --define "__signature gpg" --define "_gpg_name $(DEFAULT_KEY)" --addsign $(PACKAGE)*.rpm ; touch stamp-rpmsign; fi

rpmsign: stamp-rpmsign
 
clean:
	rm -rf $(BUILDDIR)
	rm -f stamp-*
 
distclean: clean
	rm -f *.rpm
