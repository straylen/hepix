\documentstyle[mprocl,psfig,html,longtable,epsfig,rotating]{article}

\bibliographystyle{unsrt}    % for BibTeX - sorted numerical labels by order of
                             % first citation.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                %
%    BEGINNING OF TEXT                           %
%                                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\title{Common HEP UNIX Environment}

\author{ Arnaud Taddei \footnote{\hspace{2mm}on behalf of the HEPiX-scripts UMTF-X11
and HEPiX-X11 Working Groups}}

\address{CERN, CN Division, DCI Group\\
CH 1211 Gen\`{e}ve 23, SWITZERLAND}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% You may repeat \author \address as often as necessary      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\maketitle
\abstracts{After it had been decided to design a common user
environment for UNIX platforms among HEP
laboratories, a joint project between DESY and CERN
had been started. The project consists in 2 phases: \\
\\
   1. Provide a common user environment at shell level, \\
   2. Provide a common user environment at graphical
     level (X11). \\
\\
Phase 1 is in production at DESY and at CERN as
well as at PISA and RAL. It has been developed around the
scripts originally designed at DESY Zeuthen improved and extended
with a 2 months project at CERN with a contribution
from DESY Hamburg. It consists of a set of files which
are customizing the environment for the 6 main shells (sh,
csh, ksh, bash, tcsh, zsh) on the main platforms (AIX,
HP-UX, IRIX, SunOS, Solaris2, OSF/1, ULTRIX, etc.)
and it is divided at several "sociological" levels: HEP,
site, machine, cluster, group of users and user with some
levels which are optional.\\ 
\\
The second phase is under design and a first proposal has
been published. A first version of the phase 2 exists already
for AIX and Solaris, and it should be available for all 
other platforms, by the time of the conference.\\
\\
This is a major collective work between several HEP
laboratories involved in the HEPiX-scripts and
HEPiX-X11 working-groups.\\
}

\section{Introduction and Background}
When a user interacts with a system, he has to deal with mainly 4 
kinds of interfaces: 
\begin{itemize}
\item shells ({\bf sh}, {\bf csh}, {\bf ksh}, {\bf bash}, {\bf tcsh}, 
      {\bf zsh})
\item graphical system (X11)
\item applications ({\bf pine}, {\bf emacs}, etc.)
\item connectivity tools ({\bf rsh}, {\bf xrsh}, {\bf telnet}, etc.)
\end{itemize}

In UNIX systems, many shells, a graphical
system namely X11, many applications which can be customized 
in several ways and eventually connectivity issues between systems 
make the life uneasy for a end user.

Facing this complex situation, the HEPiX committee (High Energy Physics 
UNIX committee), decided to address it in order to simplify the user
environment up to a manageable level.

Thus, the HEPiX scripts project had been launched and this paper describes the
motivations, and some components 
from the work which mainly involved the DESY UNIX Committee (DUX) and
CERN itself. This project is meant to have a more homogeneous environment
in analogy to the HEPVM efforts on the IBM mainframes. 

So, one goal is to keep differences between platforms as small as possible
and the challenge is to ensure that all combinations of platforms and software 
products represent a reliable and consistent user environment.\\
\\
These scripts are setting up a Common Unix Environment at shell 
and graphical level (X11) (available in beta version now).

At shell level, they provide the default environment for users to login, work 
in interactive and batch mode for the most common shells and platforms.

At X11 level, they provide the default environment for users to login from 
an Xserver (X terminal, Workstation, PC, etc.) to a UNIX system with a 
properly set up "Xsession".

Thus each UNIX user should be able to work on any platform in the HEP
community without having to reset his environment files.\\
\\
In order to reach this amitious goal, DESY Hamburg and DESY Zeuthen started
the initial work on the shell level in 1992. They produced the main engine
and the main background ideas. Then a joint project between DESY Zeuthen 
and CERN had been done between February and end of March 1994. Since then
\begin{itemize}
\item the HEPiX scripts at shell level had been deployed at CERN, RAL, INFN,
\item the HEPiX scripts group worked on the X11 specifications and 
      implementation, and new work had been produced between DESY Hamburg, 
      DESY Zeuthen and CERN.
\end{itemize}




\section{Components}

\subsection{Overview of the components}
The HEPiX scripts are a set of files which you can install on your system in
different modes. These files provides settings for shell configurations
and in a near future for X11 environment configurations. Moreover, the "HEPiX
package" contains documentation for users, system administrators, etc.
who want to use and/or customize the environment. Besides the documentation,
it provides templates for the user dot-files and tools like the {\bf uco} 
command which can be used as a "reset button" for users to reset their 
environment to system defaults. At least, in order to provide some 
Software Quality Assurance, the HEPiX scripts are compiled, available in debug
mode, optimized for each target, post-processed for better readability.

\subsection{Shells}
The shells are the "oldest" interface with the system. When you want to 
interact with an operating system (not restricted to UNIX) you enter commands
in the "shell" which executes them. In UNIX it exists many shells and the
most classical ones are: {\bf sh}, {\bf csh}, {\bf ksh}, {\bf bash}, 
{\bf tcsh}, {\bf zsh}. \cite{taddei.shellchoice} gives a comparison 
between them. The problem is that they have many different behaviors, unequal
support, sometime important deficiencies and bugs ({\bf sh}, {\bf ksh}, 
{\bf csh}).

Any improvement to it can result in considerably more effective use of the 
system.

The HEPiX scripts support all of these shells and enable by default, 
the most advanced features of each of them.
Command line editing, shell history and completion mechanism are some examples
of such features.
Moreover, they provide some key-bindings, they define few common aliases
and environment variables are set properly.

The shell status is detected and specific environments are provided according
to it. If the shell is a login shell, some additional files may be started
and thus the login environment is calculated once. In interactive mode, 
some environment is provided as well as in any non-interactive environment
(sub-shells, batch, etc.). The environment is still correct after changing the
shell, after exiting from an "{\bf su}" session or for remote commands like
{\bf rsh}.

\subsection{levels}

More fundamental is the role that the HEPiX scripts are playing as a tool 
which can be customized by various classes of system administrators. In 
large organizations such as CERN it had been necessary to distinguish among
the system administrators.

Indeed a system administrator can be a user in front of a workstation,
somebody responsible of a group of users, 
somebody responsible of a group of machines, a manager 
who has political responsibility, 
who wants to implement some services (improve the security)
please some users communities.

Thus, the HEPiX scripts provide hooks or anchors for some predefined
files which can be customized by any of these system administrators. 
Figure \ref{mechanical} shows the "route" which is used by the shell to get
its configuration (this model is still valid for X11).

\begin{latexonly}
\begin{figure}
\caption{Levels - Mechanical aspects} \label{mechanical}
\makebox{\resizebox{\textwidth}{!}{\rotatebox{0}{\includegraphics{fig/mechanical-levels-light.ps}}}}
\end{figure}
\end{latexonly}
\begin{rawhtml}
<img src="http://wwwcn.cern.ch/hepix/wg/scripts/doc/chep95/fig/mechanical-levels-light.gif">
\end{rawhtml}

A key point on Fig.\ref{mechanical} is to observe that there are 
two installation modes.
The "{\it weak}" mode which doesn't replace any vendor system file and which is
meant for all machines which provide "{\it old}" services. 
For these machines it
would be probably not very convenient to enforce a new environment for 
their users. The "{\it enforced}" mode is used for all new machines, 
services, users
and group of users. Thus, they get a default HEPiX environment. This
mechanism gives the flexibility to migrate slowly services to the HEPiX 
environment.
Moreover, it should be pointed out on the fact that {\tt root} and all users
with a {\tt uid} less than 100 are not getting the HEPiX environment by 
default. This is shown with the anchor to the box "{\it Vendor files}" 
on Fig.\ref{mechanical}.

\subsection{X11}

This paper is not the correct place to describe the HEPiX X11 environment as
the list of features described is too long. 
This is described elsewhere \cite{www.shells}, 
\cite{www.umtfX11}. The X11 part is in beta version and is working on 
all the supported platforms now. The work is done on the documentation and 
communications.

\section{Project status \& Future Plans}

This project started in 1992 with an initial research and development 
lead by DESY Hamburg then Zeuthen. At the beginning of 1994, A. K\"ohler/DESY
came to CERN and, together with the author, they improved the scripts at shell
level, provided documentation, started the initial deployment. After the 
initial feedback at CERN, new developments of the HEPiX scripts had been 
undertaken and started the major deployment at CERN during winter 1994-1995.
In the same time, RAL and INFN deployed the HEPiX scripts on their
respective sites.

Although the scripts have been really well accepted, it became clear that 
solving the shell problem was not enough and 2 working-groups
had been set up, UMTF-X11 at CERN and HEPiX-X11, in order to specify and then
propose a first implementation. 

Fig.\ref{history} summarizes the history of the project. 
The cooperation between DESY and CERN and the 
feedback from INFN, RAL, Saclay and Slac is really a key-factor of success. 
Many people contributed to provide users, a decent defaults and system 
managers, easy ways to make local adaptations.

\begin{latexonly}
\begin{figure}
\caption{Project history} \label{history}
\makebox{\resizebox{\textwidth}{!}{\rotatebox{0}{\includegraphics{fig/hepix-history.ps}}}}
\end{figure}
\end{latexonly}
\begin{rawhtml}
<img src="http://wwwcn.cern.ch/hepix/wg/scripts/doc/chep95/fig/hepix-history.gif">
\end{rawhtml}

At CERN it should be noticed that the HEPiX scripts are deployed on all the
WorkGroup Server (WGS) platforms and PLUS machines. Thus, CMS, ATLAS, SHITCMS,
 SHIFTATLAS,
CERNSP, HPPLUS and also traditional UNIX services like DXCERN provide the HEPiX
environment. The integration with the other services had been successful, in
particular, with SUE, ASIS and CORE.




\section{Conclusion}

The HEPiX scripts provide a very powerful paradigm. They provide decent
defaults, they allow a lot of evolutions and meet migration requirements.
They show a very good acceptance level, at CERN and in other HEP laboratories. 
Eventually, they generated the development of a HEP-wide X11-environment
and they will allow us to integrate and support a lot of applications.

The shell part is considered to be finished and we definitely are in its
maintenance phase. The X11 part of the HEPiX scripts is available in beta
version and is under restricted deployment at CERN 
and should be available in a near-future.



\section{Acknowledgments}
Among the many people involved in the HEPiX-scripts, the HEPiX-X11
and UMTF-X11 working-groups, my special thanks to A. K\"ohler,
T. Finnern and L. Cons for their knowledge and advices.
Also my many thanks to our respective managers who gave us the 
required political support.



\section*{References}
\begin{thebibliography}{99}
\bibitem{www.shells}{\em HEPiX Scripts welcome page}, Arnaud Taddei,\\
             \htmladdnormallink{{\tt http://wwwcn.cern.ch/hepix/wg/scripts/www/Welcome.html}}{http://wwwcn.cern.ch/hepix/wg/scripts/www/Welcome.html}
\bibitem{www.umtfX11}{\em UMTF Working-groups welcome page}, Miguel Marquina 
             and Arnaud Taddei, \\
             \htmladdnormallink{{\tt http://wwwcn.cern.ch/umtf/wg}}{http://wwwcn.cern.ch/umtf/wg}
\bibitem{taddei.shellchoice}Shell-Choice - A Shell Comparison, Arnaud Taddei\\
                CERN Writeup: CERN/CN/UCO/162 \\
                \htmladdnormallink{{\tt http://consult.cern.ch/writeup/shellchoice}}{http://consult.cern.ch/writeup/shellchoice}.
\bibitem{taddei}Proposal for a Common Script and X environment at DESY
                and at CERN, Axel K\"ohler and Arnaud Taddei.\\ 
                CERN Writeup: CERN/CN/UCO/168 \\
                \htmladdnormallink{{\tt http://consult.cern.ch/writeup/hepscripts}}{http://consult.cern.ch/writeup/hepscripts}
\end{thebibliography}
\end{document}



