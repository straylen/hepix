Common HEP UNIX Environment
***************************


After it had been decided to design a common user environment
for UNIX platforms among HEP laboratories, a joint project
between DESY and CERN had been started. The project consists
in 2 phases: 

 1. provide a common user environment at shell level, 
 2. provide a common user environment at graphical level. 

Phase 1 is in production at DESY and at CERN as well as at
PISA. It had been developped around the scripts designed at
DESY Zeuthen and improved and extended with a 2 months
project at CERN with a contribution from DESY Hamburg. It
consists of a set of files which customise the environment for the
6 main shells (sh, csh, ksh, bash, tcsh, zsh) for the main platforms
(AIX, HP-UX, IRIX, SunOs, Solaris2, OSF/1, ULTRIX, etc.)
and it is divided into several "sociological" levels: HEP, site,
machine, cluster, group of users and user with some levels which
are optional. 

Phase 2 is in deployment and is part of this 3.0.* version. It
includes many new features in the X11 area. Consult the Version
file for a full description. This part is now deployed in
production on the PLUS services at CERN and at PISA. 

If your system runs make_asis or ASISUpdate you should find
the files under /usr/local/lib/hepix. Templates are provided under
/usr/local/lib/hepix/templates and some external documents
refered as number 162, 163, 170, 171, 172, 184, 185 are available
from the UCO. 

The main entry point for the HEPiX scripts project is located at: 

        http://wwwcn.cern.ch/hepix/wg/scripts/www/Welcome.html

Contact: Arnaud.Taddei@cern.ch 

Arnaud Taddei, 22-Nov-1995 
