This is the "README" file for the HEPiX package version $HEP_VERSION

Author of this document: $HEP_WRAPPERS_AUTHORS

This package contains a collection of scripts, programs and documents as 
well as an installer program named "make.pl": 

Scripts and programs:
---------------------
The scripts and programs are provided in 2 production modes: 
   1) the so-called "inpro" mode which means "in production" and
      is the recommended mode to be installed. All the files under directory
      "inpro" are in mode "inpro".
   2) the so-called "debug" mode which means that at run time the programs and
      the scripts will log information in the /tmp directory.
      This mode is not recommended and is provided for you to trace, analyse
      and understand what the scripts and programs are doing or not doing. 
      It is NOT recommended to install this mode on a production service. 
      All the files are under the directory "debug".

Documentation:
--------------
Several documents are available for you:
   1) README                              
      This document
   2) Architectural Design Documentation      --- Not Ready Yet ---
      The document which contains the design ideas and motivations: 
      What is available today.
   3) Project Management Document             --- Not Ready Yet ---
      The document which contains the project management issues: 
      What had been done in the past
   4) Todo list                               --- Not Ready Yet --- 
      The "Todo" list: 
      What has to be done in the future
   5) Reference Guide                         --- Not Ready Yet --- 
      The reference which contains all the details.
   6) User Guide                          
      How a user can use and customise his environment.
   7) System Administrator Guide          
      How to install and customise the HEPiX scripts for a system, a site,
      a cluster of machines or a group of users.
   8) Copyright                          
      The legal aspects
   9) Version                            
      A basic description of the current version 3.0.
      (file propagated by the CERN ASIS system)
  10) Description                        
      A description of the HEPiX scripts version 3.0.
      (file propagated by the CERN ASIS system)
   

All these documents are under the repository "doc". In the main repository, 
you have:

   README      --> doc/README
   Copyright   --> doc/Copyright
   Version     --> doc/Version
   Description --> doc/Description

Moreover all the documents are available via the World Wide Web at the
following location:

   http://consult.cern.ch/hepix/scripts/Welcome.html

The installer procedure:
------------------------
The program "make.pl" is a perl script which is meant to install the HEPiX 
scripts on your system. It expects that the perl interpreter in under:

   /usr/local/bin/perl

and that the standard perl libraries are under:
 
   /usr/local/lib/perl

Read document number 7 if you want to install the scripts on your system.
For a basic help, run above

   make.pl -h 

For a faked installation, run

   make.pl -n 

or 

   make.pl -n -F 1

where the -F 1 flag means that you will overwrite existing HEPiX
files or links which could come from a previous installation
from ASIS or another distribution tool. Thus it makes sure that
you want to get the HEPiX files localy.


------------------------------------------------------------
Arnaud Taddei			tel : +41 22 767 93 49 
CN Division 31 2-013		fax : +41 22 767 71 55
CERN				mail: taddei@mail.cern.ch
CH-1211 Geneve 23
------------------------------------------------------------
