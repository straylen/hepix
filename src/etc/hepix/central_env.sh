#!/bin/sh

# This is a HEPiX entry point for non-login shells from
# user's dotfiles if HEPiX is not enforced at system level.

_HPX_SEEN_CENTRAL_ENV_SH=1

[ -z "$HPX_INIT" ] && . /etc/hepix/init.sh

hpx_is_hepix || return
hpx_source $HPX_HOME_HEP/env

# End of file.
