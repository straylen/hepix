#!/bin/csh

set _HPX_SEEN_CENTRAL_ENV_CSH=1

if ( ! $?HPX_INIT ) then
	source /etc/hepix/init.csh
endif

hpx_is_hepix
if ( $status == 0 ) then
	hpx_source $HPX_HOME_HEP/csh.cshrc
endif

# End of file.
