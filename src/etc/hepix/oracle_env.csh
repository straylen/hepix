#!/bin/csh

if ( $?_HPX_SEEN_ORACLE_ENV_CSH ) then
	exit
endif
set _HPX_SEEN_ORACLE_ENV_CSH=1

if ( ! $?HPX_INIT ) then
	source /etc/hepix/init.csh
endif

setenv ORACLE_CERN /afs/cern.ch/project/oracle
hpx_source $ORACLE_CERN/script/cshrc_oracle.csh

# End of file.
