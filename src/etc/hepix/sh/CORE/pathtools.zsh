#!/bin/sh

[ -n "$_HPX_SEEN_CORE_PATHTOOLS_ZSH" ] && return
_HPX_SEEN_CORE_PATHTOOLS_ZSH=1

# zsh(1)-specific implementations of the `pathtools' functions:
# hpx_path_is_present()
# hpx_path_remove()
# hpx_path_prepend()
# hpx_path_prepend_unique()
# hpx_path_prepend_unless_present()
# hpx_path_append()
# hpx_path_append_unique()
# hpx_path_append_unless_present()


# Check if PARAM is present in PATH as a valid entry.
# Return 0 if yes, 1 if not.
hpx_path_is_present () {
	: ${1:?"Missing pathname."}
	for _HPX_DIR in $path[@]; do
		if [ "$_HPX_DIR" = "$1" ]; then
			return 0
		fi
	done
	return 1
	unset _HPX_DIR
}

# Remove all occurrences of a given directory from $PATH.
# NOTE(fuji): Increment the counter conditionally, since if
# we found a match, deleting that entry would "pull" the
# rest of the array towards the beginning, shifting all
# indices.  Hence we have to increment the counter only if
# there was no match.
hpx_path_remove () {
	: ${1:?"Missing pathname."}
	_HPX_INDEX=1
	while [ $_HPX_INDEX -le $#path ]; do
		if [ "$path[$_HPX_INDEX]" = "$1" ]; then 
			path[$_HPX_INDEX]=()
		else
			_HPX_INDEX=$(($_HPX_INDEX+1))
		fi
	done
	unset _HPX_INDEX
}

# Prepend given directoy to PATH.
hpx_path_prepend () {
	: ${1:?"Missing pathname."}
	if [ -z "$PATH" ]; then
		PATH="$1"
	else
		PATH="$1:$PATH"
	fi
}

# Prepend given directory to PATH and remove all other
# occurrences.
hpx_path_prepend_unique () {
	: ${1:?"Missing pathname."}
	hpx_path_remove "$1"
	hpx_path_prepend "$1"
}

# Prepend given directory to PATH unless it is already
# present (anywhere) in PATH.
hpx_path_prepend_unless_present () {
	: ${1:?"Missing pathname."}
	if ! hpx_path_is_present "$1"; then
		hpx_path_prepend "$1"
	fi
}

# Append given directoy to PATH.
hpx_path_append () {
	: ${1:?"Missing pathname."}
	if [ -z "$PATH" ]; then
		PATH="$1"
	else
		PATH="$PATH:$1"
	fi
}

# Append given directory to PATH and remove all other
# occurrences.
hpx_path_append_unique () {
	: ${1:?"Missing pathname."}
	hpx_path_remove "$1"
	hpx_path_append "$1"
}

# Append given directory to PATH unless it is already
# present (anywhere) in PATH.
hpx_path_append_unless_present () {
	: ${1:?"Missing pathname."}
	if ! hpx_path_is_present "$1"; then
		hpx_path_append "$1"
	fi
}

# End of file.
