#!/bin/csh

if ( $?_HPX_SEEN_HEP_CSH_CSHRC ) then
	exit
endif
set _HPX_SEEN_HEP_CSH_CSHRC=1

if ( ! $?HPX_INIT ) then
	source /etc/hepix/init.csh
endif

# NOTE(fuji): sh(1) derivatives source `pathtools' here, but we have to
# define aliases to emulated shell functions for csh(1).  See csh/CORE/init
# for explanation.
alias hpx_path_is_present	"source $HPX_HOME_CORE/hpx_path_is_present.csh "
alias hpx_path_remove		"source $HPX_HOME_CORE/hpx_path_remove.csh "
alias hpx_path_prepend		"source $HPX_HOME_CORE/hpx_path_prepend.csh "
alias hpx_path_prepend_unique	"source $HPX_HOME_CORE/hpx_path_prepend_unique.csh "
alias hpx_path_prepend_unless_present	"source $HPX_HOME_CORE/hpx_path_prepend_unless_present.csh "
alias hpx_path_append		"source $HPX_HOME_CORE/hpx_path_append.csh "
alias hpx_path_append_unique	"source $HPX_HOME_CORE/hpx_path_append_unique.csh "
alias hpx_path_append_unless_present	"source $HPX_HOME_CORE/hpx_path_append_unless_present.csh "

umask $HPX_UMASK

# Determine HEP group name.
set HPX_PREF_GROUP=$HPX_USER_HOME/preferred-group
if ( -r $HPX_PREF_GROUP) then
	set HPX_HEPGROUP=`/bin/cat $HPX_PREF_GROUP`
else
	set HPX_HEPGROUP=`/usr/bin/id -gn`
endif
unset HPX_PREF_GROUP

# Decide whether a group script is packaged (overriding any afs/group scripts) and source it; then return
if ( -d /etc/hepix/csh/GROUP/$HPX_HEPGROUP && "$HPX_WANTS_AFS" != "true" ) then
  hpx_source "$HPX_HOME_GROUP/$HPX_HEPGROUP/group_rc.sh"
	if ( "$HPX_WANTS_AFS" == "false" ) exit
endif

if ( -d /afs/cern.ch/group/$HPX_HEPGROUP ) then
        setenv GROUP_DIR /afs/cern.ch/group/$HPX_HEPGROUP
endif
unset HPX_HEPGROUP

# Sanitize and set site-wide environment variables.

set HPX_INITIALE=`echo $USER | /usr/bin/cut -c1`
setenv CASTOR_HOME /castor/cern.ch/user/$HPX_INITIALE/$USER
unset HPX_INITIALE

# $EDITOR is used by SVN, crontab, etc
setenv EDITOR "/bin/nano -w"

# NOTE(fuji): These are normally in the local namespace, but AFS
# group script maintainers tend to `setenv' these variables and
# then we have a namespace clash (no way to specify at dereference
# ($GROUPPATH) time which namespace to access.  See also ROOTPATH
# assignments.
# CT197070 Matthias Schröder <Matthias.Schroder@cern.ch>
if ( $?GROUP_DIR ) then
	setenv GROUPPATH $GROUP_DIR/bin
endif

if ( $?GROUP_DIR ) then
        hpx_source $GROUP_DIR/group_sys.conf.csh
        hpx_source $GROUP_DIR/group_env.csh
endif

if ( $?GROUPPATH ) then
	echo "$GROUPPATH" | /bin/grep ' ' >/dev/null
	if ( $status == 0 ) then
		hpx_debug "GROUPPATH contains obsolete space separator, use colons."
		setenv GROUPPATH `echo "$GROUPPATH" | /bin/sed 's/ /:/g'`
	endif
endif
if ( $?USERPATH ) then
	echo "$USERPATH" | /bin/grep ' ' >/dev/null
	if ( $status == 0 ) then
		hpx_debug "USERPATH contains obsolete space separator, use colons."
		setenv USERPATH `echo "$USERPATH" | /bin/sed 's/ /:/g'`
	endif
endif

if ( $?GROUPPATH ) then
        setenv USERPATH ${GROUPPATH}:${HOME}/scripts
else
        setenv USERPATH ${HOME}/scripts
endif

if ( $?GROUP_DIR ) then
        hpx_source $GROUP_DIR/group_aliases.csh
        hpx_source $GROUP_DIR/group_csh.cshrc
endif

# Incorporate assembled entries into PATH.
set HPX_PATH=$USERPATH

# STEP 0. Append PATH to HPX_PATH and empty PATH, we're going
# to iterate over HPX_PATH and uniquely append to an (initially
# empty) PATH.

set HPX_PATH=${HPX_PATH}:${PATH}
setenv PATH ""

# STEP 1. Create a space-separated list from HPX_PATH using the shell's
# PATH<->path synchronization.
set _HPX_PATH_SAVE=$PATH
setenv PATH $HPX_PATH
set hpx_path=($path)
setenv PATH $_HPX_PATH_SAVE
unset _HPX_PATH_SAVE

# STEP 2. Emulate what hpx_path_append_unique() does in sh(1)-derivatives.
# NOTE(fuji): Let's try with our shiny pathtools implementation.
foreach HPX_DIR ($hpx_path)
## 	set c=1
## 	set found=0
## 	while ( ! $found && $c <= $#path )
## 		if ( $HPX_DIR == "$path[$c]" ) then
## 			set found=1
## 		endif
## 		@ c++
## 	end
## 	if ( ! found ) then
## 		set path=($path $HPX_DIR)
## 	endif
	eval `hpx_path_append_unless_present "$HPX_DIR"`
end
## unset c
## unset found

# STEP 3. Release outer level temporary variables, we're done.
unset HPX_DIR
unset hpx_path
unset HPX_PATH

unset USERPATH
unset GROUPPATH

set _HPX_SHELL=${SHELL:t}
hpx_source $HPX_HOME_HEP/${_HPX_SHELL}rc
unset _HPX_SHELL

# End of file.
