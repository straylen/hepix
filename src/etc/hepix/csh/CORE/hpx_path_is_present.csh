#!/bin/csh

if ( "$1" == "" ) then
	echo "hpx_path_is_present(): Missing pathname."
	exit
endif

# Check if PARAM is present in PATH as a valid entry.
# Return 0 if yes, 1 if not.

foreach _HPX_DIR ($path)
	if ( "$_HPX_DIR" == "$1" ) then
		exit 0
	endif
end
unset _HPX_DIR
exit 1

# End of file.
