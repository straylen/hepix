#!/bin/csh

if ( "$1" == "" ) then
	echo "hpx_path_append_unique(): Missing pathname."
	exit
endif

# Append given directory to PATH unless it is already
# present (anywhere) in PATH.
# NOTE(fuji): Caller should eval() the returned result!

hpx_path_is_present "$1"
if ( $status ) then
	eval `hpx_path_append "$1"`
endif

echo "setenv PATH $PATH"

# End of file.
