#!/bin/csh

if ( ! $?HPX_INIT ) then
	source /etc/hepix/init.csh
endif

set _HPX_TKLIFE=`$HPXB_ALIFETIME`
if ( $_HPX_TKLIFE == 0 ) then
	hpx_echo ">>>>> AFS token expired! <<<<<"
else if ( $_HPX_TKLIFE < 3600 ) then
	set _HPX_TKLIFE=`/usr/bin/expr $_HPX_TKLIFE / 60`
	hpx_echo ">>>>> AFS token is going to expire in $_HPX_TKLIFE minutes <<<<<"
endif
unset _HPX_TKLIFE

# End of file.
