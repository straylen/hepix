#!/bin/csh

if ( ! $#argv ) then
	exit
endif
if ( $?HPX_VERBOSE ) then
	if ( $HPX_VERBOSE >= 13 ) then
		hpx_echo "N: CHECK $1"
	endif
	if ( ! -r $1 ) then
		exit
	endif
	if ( $HPX_VERBOSE >= 11 ) then
		hpx_echo "N: -> SOURCE $1"
	endif
	source $1
	if ( $HPX_VERBOSE >= 12 ) then
		hpx_echo "N: PROCESSED $1"
	endif
endif
		
# End of file.
